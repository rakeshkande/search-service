package com.micro.services.search.api.response;

import java.io.Serializable;

public enum BreadCrumbTrailType implements Serializable {
    SEARCH,
    NAVIGATION
}
